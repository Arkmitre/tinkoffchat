//
//  MessageCell.swift
//  TinkoffChat
//
//  Created by Alexander on 08/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

protocol MessageCellConfiguration: class {
    var message: String? { get set }
}

class MessageCell: UITableViewCell, MessageCellConfiguration {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    var message: String? {
        didSet {
            messageLabel.text = message
        }
    }
}
