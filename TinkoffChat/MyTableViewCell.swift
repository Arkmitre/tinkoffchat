//
//  MyTableViewCell.swift
//  TinkoffChat
//
//  Created by Alexander on 08/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

protocol ConversationCellConfiguration: class {
    var name: String? { get set }
    var message: String? { get set }
    var date: Date? { get set }
    var online: Bool { get set }
    var hasUnreadMessages: Bool { get set }
}

class MyTableViewCell: UITableViewCell, ConversationCellConfiguration {

    static let identifier = "MyCell"
    
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var timeMarkLabel: UILabel!

    var name: String? {
        didSet {
            contactNameLabel.text = name
        }
    }
    
    var message: String? {
        didSet {
            if message == nil {
                lastMessageLabel.text = "No messages yet"
            } else {
                lastMessageLabel.text = message
            }
        }
    }
    
    var dateFormatter = DateFormatter()
    var date: Date? {
        didSet {
            guard let date = date else {
                timeMarkLabel.text = nil
                return
            }
            if Calendar.current.isDateInToday(date) {
                
                dateFormatter.dateFormat = "HH:mm"
                
            } else {
                
            dateFormatter.dateFormat = "dd MMM"
            }
            
            timeMarkLabel.text = dateFormatter.string(from: date)
        }
    }
    
    var online: Bool = false {
        didSet {
            if online {
                
            contentView.backgroundColor = UIColor.init(hue: 0.1667, saturation: 0.19, brightness: 0.97, alpha: 1.0)
            
            } else { contentView.backgroundColor = UIColor.white }
        }
    }
    
    var hasUnreadMessages: Bool = false {
        didSet {
            if hasUnreadMessages {
                
            lastMessageLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
            
            } else { lastMessageLabel.font = UIFont.systemFont(ofSize: 17, weight: .regular) }
        }
    }
}
