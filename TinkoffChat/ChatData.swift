//
//  SomeModelDataSource.swift
//  TinkoffChat
//
//  Created by Alexander on 08/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation

class ChatData {
    let user: UserId
    var messages: [MessageData]
    var online: Bool
    let hasUnreadMessages: Bool
    
    init(user: UserId,
         messages: [MessageData] = [],
         online: Bool = true,
         hasUnreadMessages: Bool = false) {
        self.user = user
        self.messages = messages
        self.online = online
        self.hasUnreadMessages = hasUnreadMessages
    }
}
