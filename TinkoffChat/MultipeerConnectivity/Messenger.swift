//
//  Messenger.swift
//  TinkoffChat
//
//  Created by Alexander on 28/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import MultipeerConnectivity

protocol Communicator: class {
    func sendMessage(text: String, to user: UserId) -> Bool
    var delegate: CommunicatorDelegate? { get set }
}

protocol CommunicatorDelegate: class {
    func didFoundUser(_ communicator: Communicator, user: UserId)
    func didLostUser(_ communicator: Communicator, user: UserId)
    func didReceiveMessage(_ communicator: Communicator, text: String, user: UserId)
    func didFailed(_ communicator: Communicator, with error: Error)
}

//

class Messenger: NSObject, Communicator {
    
    weak var delegate: CommunicatorDelegate?
    
    private let serviceType = "tinkoff-chat"
    private let myPeer = MCPeerID(displayName: UIDevice.current.name)
    private let advertiser: MCNearbyServiceAdvertiser
    private let browser: MCNearbyServiceBrowser
    
    private var sessions: [NSObject: MCSession] = [:]
    private var userNames: [MCPeerID: String] = [:]
    
    init(userName: String) {
        advertiser = MCNearbyServiceAdvertiser(peer: myPeer,
                                               discoveryInfo: ["userName": userName],
                                               serviceType: serviceType)
        browser = MCNearbyServiceBrowser(peer: myPeer, serviceType: serviceType)
        
        super.init()
        
        advertiser.delegate = self
        browser.delegate = self
        
        advertiser.startAdvertisingPeer()
        browser.startBrowsingForPeers()
    }
    
    func sendMessage(text: String, to user: UserId) -> Bool {
        guard let session = sessions[user.uid] else {
            return false
        }
        
        let message = ["text": text]
        do {
            let data = try JSONSerialization.data(withJSONObject: message)
            try session.send(data, toPeers: [user.uid as! MCPeerID], with: .reliable)
            return true
        } catch {
            delegate?.didFailed(self, with: error)
            return false
        }
    }
}

extension Messenger: MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser,
                    didReceiveInvitationFromPeer peerID: MCPeerID,
                    withContext context: Data?,
                    invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        let session = sessions[peerID, default: MCSession(peer: myPeer)]
        sessions[peerID] = session
        session.delegate = self
        invitationHandler(true, session)
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser,
                    didNotStartAdvertisingPeer error: Error) {
        delegate?.didFailed(self, with: error)
    }
}

extension Messenger: MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser,
                 foundPeer peerID: MCPeerID,
                 withDiscoveryInfo info: [String : String]?) {
        let session = sessions[peerID, default: MCSession(peer: myPeer)]
        sessions[peerID] = session
        session.delegate = self
        
        userNames[peerID] = info?["userName"]
        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 30.0)
        
        let user = UserId(uid: peerID, name: userNames[peerID] ?? "None")
        delegate?.didFoundUser(self, user: user)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        let user = UserId(uid: peerID, name: userNames[peerID] ?? "None")
        delegate?.didLostUser(self, user: user)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        delegate?.didFailed(self, with: error)
    }
}

extension Messenger: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        //
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        do {
            let json = try JSONSerialization.jsonObject(with: data)
            let dictionary = json as? [String: String]
            if let text = dictionary?["text"] {
                let user = UserId(uid: peerID, name: userNames[peerID] ?? "None")
                
                DispatchQueue.main.async {
                    self.delegate?.didReceiveMessage(self, text: text, user: user)
                }
            }
        } catch {
            DispatchQueue.main.async {
                self.delegate?.didFailed(self, with: error)
            }
        }
    }
    
    func session(_ session: MCSession,
                 didReceive stream: InputStream,
                 withName streamName: String,
                 fromPeer peerID: MCPeerID) {}
    
    func session(_ session: MCSession,
                 didStartReceivingResourceWithName resourceName: String,
                 fromPeer peerID: MCPeerID,
                 with progress: Progress) {}
    
    func session(_ session: MCSession,
                 didFinishReceivingResourceWithName resourceName: String,
                 fromPeer peerID: MCPeerID,
                 at localURL: URL?,
                 withError error: Error?) {}
}
