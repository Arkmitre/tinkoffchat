//
//  Storage.swift
//  TinkoffChat
//
//  Created by Alexander on 28/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import CoreData



protocol Storage: class {
    func name() -> String
    var chats: [ChatData] { get set }
    var userProfile: UserProfile? { get }
    func chat(by user: UserId) -> ChatData?
    func setUserName(_ userName: String?, andInfo info: String?)
}

struct UserId {
    let uid: NSObject
    let name: String
}

class DatabaseStorage: Storage {
    
    var userProfile: UserProfile? {
        return try? dataStorage.readUserProfile(context: dataStorage.mainContext)
    }
    
    func setUserName(_ userName: String?, andInfo info: String?) {
        dataStorage.saveContext.perform {
            let userProfile = try? self.dataStorage.readUserProfile(context: self.dataStorage.saveContext)
            userProfile?.name = userName
            userProfile?.info = info
            self.dataStorage.performSave(with: self.dataStorage.saveContext)
        }
    }
    
    func name() -> String {
        return userProfile?.name ?? "Unknown"
    }

    private let dataStorage = CoreDataStorage()
    
    var chats: [ChatData] = []
    
    func chat(by user: UserId) -> ChatData? {
        for chat in chats {
            if chat.user.uid == user.uid {
                return chat
            }
        }
        return nil
    }
}
