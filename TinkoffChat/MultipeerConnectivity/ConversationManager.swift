//
//  ConversationManager.swift
//  TinkoffChat
//
//  Created by Alexander on 28/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation

protocol ConversationManagerDelegate: class {
    func conversationManagerDidChange(_ conversationManager: ConversationManager)
    func conversationManagerDidFailed(_ conversationManager: ConversationManager, with error: Error)
}

class ConversationManager: CommunicatorDelegate {
    
    weak var delegate: ConversationManagerDelegate?
    
    private let messenger: Messenger
    private var dataStorage: Storage
    
    init(dataStorage: Storage) {
        self.dataStorage = dataStorage
        
        messenger = Messenger(userName: dataStorage.name())
        messenger.delegate = self
    }
    
    func sendMessage(text: String, to user: UserId) -> Bool {
        let success = messenger.sendMessage(text: text, to: user)
        if success {
            let chat = dataStorage.chat(by: user)
            let message = MessageData(message: text, isIncoming: false, date: Date())
            chat?.messages.append(message)
            delegate?.conversationManagerDidChange(self)
        }
        return success
    }
    
    func didFoundUser(_ communicator: Communicator, user: UserId) {
        if let chat = dataStorage.chat(by: user) {
            chat.online = true
        } else {
            let chat = ChatData(user: user)
            dataStorage.chats.append(chat)
        }
        delegate?.conversationManagerDidChange(self)
    }
    
    func didLostUser(_ communicator: Communicator, user: UserId) {
        let chat = dataStorage.chat(by: user)
        chat?.online = false
        delegate?.conversationManagerDidChange(self)
    }
    
    func didReceiveMessage(_ communicator: Communicator, text: String, user: UserId) {
        let chat = dataStorage.chat(by: user)
        let message = MessageData(message: text, isIncoming: true, date: Date())
        chat?.messages.append(message)
        delegate?.conversationManagerDidChange(self)
    }
    
    func didFailed(_ communicator: Communicator, with error: Error) {
        delegate?.conversationManagerDidFailed(self, with: error)
    }
}
