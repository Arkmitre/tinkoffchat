//
//  ImageCollectionViewController.swift
//  TinkoffChat
//
//  Created by Alexander on 26/11/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

class ImageCollectionViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate  {
    
    let reuseIdentifier = "cell"
    var images: [URL] = []
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (self.view.frame.width - 20) / 3
        return CGSize(width: width, height: width)
    }
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MyCollectionViewCell
       cell.cellImage = nil
        loadImage(url: images[indexPath.row], completion:{ image in })
    
        //cell.cellImage.image = image

//       DispatchQueue.global().async {
//
////            let imageString = self.images[indexPath.row]
////            let imageUrl = NSURL(string: imageString)
////        let imageData = NSData(contentsOfURL: imageUrl! as URL)
////
////        if(imageData != nil)
////        {
////            cell.cellImage.image = UIImage(data: imageData!)
////            cell.cellImage.contentMode = UIViewContentMode.scaleAspectFit
////        }
//
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    
    func load(completion: @escaping ([URL]) -> Void) {
//        collectionView(<#T##collectionView: UICollectionView##UICollectionView#>, willDisplay: <#T##UICollectionViewCell#>, forItemAt: <#T##IndexPath#>)
        let url = URL(string: "https://pixabay.com/api/?key=10808249-600e2dce9e7af8b416a9371cc&q=cats&image_type=photo&per_page=200")!
        let request = URLRequest(url: url)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {data, response, error in
            guard let data = data else { return }
            
            let json = try! JSONSerialization.jsonObject(with: data) as! [String: AnyObject]
            let hits = json["hits"] as! [[String: AnyObject]]
            let imageURLs = hits
                .map { $0["webformatURL"] as! String }
                .map { URL(string: $0)! }
            DispatchQueue.main.async {
                completion(imageURLs)
            }
        })
        task.resume()
    }
    
    func loadImage(url: URL, completion: @escaping (UIImage?) -> Void) -> URLSessionTask {
        let request = URLRequest(url: url)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            guard let data = data else { return }
            guard let image = UIImage(data: data) else { return }
            
            DispatchQueue.main.async {
                completion(image)
            }
        })
        task.resume()
        
        return task
    }
    
    
}
