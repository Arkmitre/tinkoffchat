//
//  MyCollectionViewCell.swift
//  TinkoffChat
//
//  Created by Alexander on 26/11/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var cellImage: UIImageView!
}
