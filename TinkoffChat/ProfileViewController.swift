//
//  ProfileViewController.swift
//  TinkoffChat
//
//  Created by Alexander on 01/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    var dataStorage: Storage?
    
    @IBOutlet weak var profileInfoField: UITextView!
    @IBOutlet weak var profileNameField: UITextField!
    @IBOutlet weak var editImageButton: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var saveGcd: UIButton!
    @IBOutlet weak var saveOperation: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    // @IBOutlet weak var scrollView: UIScrollView!
    
    private var profileSaveLoadManager: ProfileSaveLoadManager = GCDDataManager()
    
    var nameIsEdit = false
    var infoIsEdit = false
    var imageIsEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileInfoField.delegate = self
        profileNameField.delegate = self
        
        activityIndicator.startAnimating()
        GCDDataManager().load { _, _, userAvatar in
            self.userImage.image = userAvatar ?? #imageLiteral(resourceName: "placeholderUser")
            self.activityIndicator.stopAnimating()
        }
        
        let userProfile = dataStorage?.userProfile
        profileNameField.text = userProfile?.name
        profileInfoField.text = userProfile?.info
    
        editButton.layer.cornerRadius = 10.0
        editButton.layer.masksToBounds = true
        
        editImageButton.layer.cornerRadius = 10.0
        editImageButton.layer.masksToBounds = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileInfoField.setContentOffset(.zero, animated: false) // ?
    }
    
    @IBAction func editProfileButton(_ sender: Any) {
        editMode(state: true)
    }
    
    @IBAction func saveOperationButtonAction(_ sender: Any) {
        profileSaveLoadManager = OperationDataManager()
        saveProfile()
    }
    
    @IBAction func saveGcdButtonAction(_ sender: Any) {
        profileSaveLoadManager = GCDDataManager()
        saveProfile()
    }
    
    @IBAction func nameDidChange(_ sender: UITextField) {
        nameIsEdit = true
        saveButtonsActive()
    }
    
    @IBAction func closeProfileButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction func editImageButton(_ sender: Any) {
        let actionSheetController = UIAlertController(title: "Изменить фото",
                                                      message: nil,
                                                      preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Отмена", style: .cancel)
        
        let galleryButton = UIAlertAction(title: "Галерея", style: .default) { _ in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            self.present(imagePicker, animated: true)
        }
        let loadButton = UIAlertAction(title: "Загрузить", style: .default, handler: {(action) -> Void in
//            let collectionPicker = ImageCollectionViewController()
            self.performSegue(withIdentifier: "imageCollection", sender: self)
        })
        
        actionSheetController.addAction(cancelActionButton)
        actionSheetController.addAction(galleryButton)
        actionSheetController.addAction(loadButton)
        
        present(actionSheetController, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userImage.image = pickedImage
            imageIsEdit = true
            saveButtonsActive()
        }
        dismiss(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "imageCollection" {
            let navigation = segue.destination as? UINavigationController
            let collectionView = navigation?.topViewController as? ImageCollectionViewController
            // ?
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        infoIsEdit = true
        saveButtonsActive()
    }
    
    func editMode(state: Bool) { //    Режим редактирования

        editButton.isHidden = state
        saveGcd.isHidden = !state
        saveOperation.isHidden = !state
        editImageButton.isHidden = !state
        profileNameField.isUserInteractionEnabled = state
        profileInfoField.isEditable = state
        
        saveGcd.isEnabled = false
        saveOperation.isEnabled = false
        
    }
    
    func loadMode(state: Bool) { //   Режим загрузки
        
        saveGcd.isEnabled = !state
        saveOperation.isEnabled = !state
        editImageButton.isEnabled = !state
        
    }
    
    func saveButtonsActive() {  // Активация кнопок для Сохранения
        saveGcd.isEnabled = true
        saveOperation.isEnabled = true
    }
    
    private func saveProfile() {
        editMode(state: false)
        
        if imageIsEdit {
            loadMode(state: true)
            activityIndicator.startAnimating()
            profileSaveLoadManager.save(userName: nil, userInfo: nil, userAvatar: userImage.image, completion: {
                self.loadMode(state: false)
                self.activityIndicator.stopAnimating()
                self.imageIsEdit = false
            })
        }
        
        if nameIsEdit || infoIsEdit {
            dataStorage?.setUserName(profileNameField.text, andInfo: profileInfoField.text)
            infoIsEdit = false
            nameIsEdit = false
        }
    }
}
