//
//  AppDelegate.swift
//  TinkoffChat
//
//  Created by Alexander on 01/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}
