//
//  ConversationViewController.swift
//  TinkoffChat
//
//  Created by Alexander on 08/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

struct MessageData {
    let message: String
    let isIncoming: Bool
    let date: Date
}

private extension MessageData {
    
    var identifier: String {
        return isIncoming ? "InCell" : "OutCell"
    }
}

class ConversationViewController: UIViewController, UITableViewDataSource {
    
    var user: UserId?
    var dataStorage: Storage?
    var conversationManager: ConversationManager?
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBAction func sendAction() {
        guard let user = user, let text = messageTextField.text, let manager = conversationManager else {
            return
        }
        
        if manager.sendMessage(text: text, to: user) {
            messageTextField.text = nil
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateSendButton(animated: false)
        animateTitleName(animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTextField.delegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let keyboardFrameValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue
        if let keyboardRect = keyboardFrameValue?.cgRectValue {
            bottomConstraint.constant = keyboardRect.height
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        bottomConstraint.constant = 0.0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let user = user else { return 0 }
        return dataStorage?.chat(by: user)?.messages.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let user = user,
            let item = dataStorage?.chat(by: user)?.messages[indexPath.row],
            let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier,
                                                     for: indexPath) as? MessageCell {
            cell.message = item.message
            return cell
        }
        
        return UITableViewCell()
    }

    func animateSendButton(animated: Bool) {

        sendButton.isEnabled = conversationState()
        
        let newColor: UIColor = conversationState() ? .green : .lightGray
        self.sendButton.backgroundColor = newColor
        
        guard animated
            else { return }
        UIView.animateKeyframes(withDuration: 0.8, delay: 0.0, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.625, animations: { self.sendButton.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
            })
            UIView.addKeyframe(withRelativeStartTime: 0.625, relativeDuration: 0.275, animations: { self.sendButton.transform = CGAffineTransform.identity
            })
        })
    }
    func conversationState() -> Bool {
        var online = false
        if let user = user {
            online = dataStorage?.chat(by: user)?.online ?? false
        }
        return online

    }
    func animateTitleName(animated: Bool) {
        
        let nameTitle = UILabel()
        nameTitle.textAlignment = .center
        nameTitle.text = self.navigationItem.title
        nameTitle.sizeToFit()
        nameTitle.textColor = conversationState() ? .green : .black

        self.navigationItem.titleView = nameTitle
        
        guard animated
            else { return }
        UIView.animateKeyframes(withDuration: 2.0, delay: 0.0, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5, animations: {
                self.navigationItem.titleView?.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            })
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                self.navigationItem.titleView?.transform = CGAffineTransform.identity
            })
        })
    }
}

extension ConversationViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ConversationViewController: ConversationManagerDelegate {
    
    func conversationManagerDidChange(_ conversationManager: ConversationManager) {
        tableView.reloadData()
        animateSendButton(animated: true)
        animateTitleName(animated: true)
    }
    
    func conversationManagerDidFailed(_ conversationManager: ConversationManager,
                                      with error: Error) {
        let alert = UIAlertController(title: "Ошибка",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        let okActionButton = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okActionButton)
        present(alert, animated: true)
    }
}

