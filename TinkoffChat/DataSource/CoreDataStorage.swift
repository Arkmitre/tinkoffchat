//
//  CoreDataStorage.swift
//  TinkoffChat
//
//  Created by Alexander on 12/11/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStorage  {
    
    // MARK: - NSPersistentStoreUrl
    
    var storeUrl: URL {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory,
                                                    in: .userDomainMask).first!
        return documentsUrl.appendingPathComponent("MyStore.sqlite")
    }
    
    // MARK: - NSManagedObjectModel
    
    let dataModelName = "StorageModel"
    let dataModelExtension = "momd"
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: self.dataModelName,
                                       withExtension: self.dataModelExtension)!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    // MARK: - NSPersistentStoreCoordinator
    
    lazy var persistenStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                               configurationName: nil,
                                               at: self.storeUrl,
                                               options: nil)
        } catch {
            assert(false, "Error adding store: \(error)")
        }
        
        return coordinator
    }()

    // MARK: - masterContext
    
    lazy var masterContext: NSManagedObjectContext = {
       var masterContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        masterContext.persistentStoreCoordinator = self.persistenStoreCoordinator
        masterContext.mergePolicy = NSOverwriteMergePolicy
        return masterContext
    }()
    
    // MARK: - mainContext
    
    lazy var mainContext: NSManagedObjectContext = {
        var mainContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        mainContext.parent = self.masterContext
        mainContext.mergePolicy = NSOverwriteMergePolicy
        return mainContext
    }()
    
    // MARK: - saveContext
    
    lazy var saveContext: NSManagedObjectContext = {
        var saveContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        saveContext.parent = self.mainContext
        saveContext.mergePolicy = NSOverwriteMergePolicy
        return saveContext
    }()

    // MARK: - SaveData
    
    typealias SaveComplition = () -> Void
    
    func performSave(with context: NSManagedObjectContext, completion: SaveComplition? = nil) {
        guard context.hasChanges else {
            completion?()
            return
        }
        
        context.perform {
            do {
                try context.save()
            } catch {
                print("Context save error: \(error)")
            }
            
            if let parentContext = context.parent {
                self.performSave(with: parentContext, completion: completion)
            } else {
                completion?()
            }
        }
    }
    
    func readUserProfile(context: NSManagedObjectContext) throws -> UserProfile {
        let request = NSFetchRequest<UserProfile>(entityName: "UserProfile")
        let users = try context.fetch(request)
        if let user = users.first {
            return user
        }
        let user = NSEntityDescription.insertNewObject(forEntityName: "UserProfile",
                                                       into: context) as! UserProfile
        return user
    }
    
    func readConversationList (context: NSManagedObjectContext) throws -> Message {
        let request = NSFetchRequest<Message>(entityName: "Message")
        let users = try context.fetch(request)
        if let user = users.first {
            return user
        }
        let user = NSEntityDescription.insertNewObject(forEntityName: "Message",
                                                       into: context) as! Message
        return user
    }
    
}
