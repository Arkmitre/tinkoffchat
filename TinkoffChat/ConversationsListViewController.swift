//
//  ConversationsListViewController.swift
//  TinkoffChat
//
//  Created by Alexander on 08/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

class ConversationsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let dataStorage: Storage = DatabaseStorage()
    private lazy var conversationManager = ConversationManager(dataStorage: dataStorage)
    private let sectionsNames = ["Online"]
    
    @IBOutlet var conversationsTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        conversationsTable.delegate = self
        conversationsTable.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        conversationManager.delegate = self
        conversationsTable.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataStorage.chats.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: MyTableViewCell.identifier,
                                                    for: indexPath) as? MyTableViewCell {
            let item = dataStorage.chats[indexPath.row]
            cell.name = item.user.name
            cell.message = item.messages.last?.message
            cell.date = item.messages.last?.date
            cell.online = item.online
            cell.hasUnreadMessages = item.hasUnreadMessages

            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionsNames[section]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDialog",
            let indexPath = conversationsTable.indexPathForSelectedRow,
            let destination = segue.destination as? ConversationViewController {
            
            let chat = dataStorage.chats[indexPath.row]
            destination.title = chat.user.name
            destination.dataStorage = dataStorage
            destination.user = chat.user
            destination.conversationManager = conversationManager
            
            conversationManager.delegate = destination
        } else if segue.identifier == "showProfile" {
            let navigation = segue.destination as? UINavigationController
            let profile = navigation?.topViewController as? ProfileViewController
            profile?.dataStorage = dataStorage
        }
    }
}


extension ConversationsListViewController: ConversationManagerDelegate {
    
    func conversationManagerDidChange(_ conversationManager: ConversationManager) {
        conversationsTable.reloadData()
    }
    
    func conversationManagerDidFailed(_ conversationManager: ConversationManager,
                                      with error: Error) {
        let alert = UIAlertController(title: "Ошибка",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        let okActionButton = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okActionButton)
        present(alert, animated: true)
    }
}
