//
//  FileManager.swift
//  TinkoffChat
//
//  Created by Alexander on 28/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

protocol ProfileSaveLoadManager {
    func load(completion: @escaping (_ userName: String?, _ userInfo: String?, _ userAvatar: UIImage?) -> Void)
    func save(userName: String?, userInfo: String?, userAvatar: UIImage?, completion: @escaping () -> Void)
}

protocol ConversationSaveLoadManager {
    func load(completion: @escaping (_ id: String?,
                                     _ name: String?,
                                     _ isOnline: Bool?,
                                     _ lastMessage: String?,
                                     _ messages: String?) -> Void)
    
    func save(id: String?,
              name: String?,
              isOnline: Bool?,
              lastMessage: String?,
              messages: String?,
              completion: @escaping () -> Void)
}


class LocalStorageProfileManager: ProfileSaveLoadManager {
    
    private var dir: URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    }
    
    private var nameStorageURL: URL? {
        return dir?.appendingPathComponent("name.txt")
    }
    
    private var infoStorageURL: URL? {
        return dir?.appendingPathComponent("info.txt")
    }
    
    private var imageStorageURL: URL? {
        return dir?.appendingPathComponent("image.png")
    }
    
    func load(completion: @escaping (_ userName: String?, _ userInfo: String?, _ userAvatar: UIImage?) -> Void) {
        var userName: String?
        var userInfo: String?
        var userAvatar: UIImage?
        
        do {
            if let imageStorageURL = imageStorageURL {
                userAvatar = UIImage(contentsOfFile: imageStorageURL.path)
            }
            
            if let nameStorageURL = nameStorageURL {
                userName = try String(contentsOf: nameStorageURL, encoding: .utf8)
            }
            
            if let infoStorageURL = infoStorageURL {
                userInfo = try String(contentsOf: infoStorageURL, encoding: .utf8)
            }
        } catch {
            print(error)
        }
        
        completion(userName, userInfo, userAvatar)
    }
    
    func save(userName: String? = nil, userInfo: String? = nil, userAvatar: UIImage? = nil, completion: @escaping () -> Void) {
        do {
            if let userName = userName, let nameStorageURL = nameStorageURL {
                try userName.write(to: nameStorageURL, atomically: true, encoding: .utf8)
            }
            
            if let userInfo = userInfo, let infoStorageURL = infoStorageURL {
                try userInfo.write(to: infoStorageURL, atomically: true, encoding: .utf8)
            }
            
            if
                let userAvatar = userAvatar,
                let data = UIImagePNGRepresentation(userAvatar),
                let imageStorageURL = imageStorageURL {
                
                try data.write(to: imageStorageURL)
            }
        } catch {
            print(error)
        }
        
        completion()
    }
}
