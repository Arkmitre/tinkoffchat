//
//  OperationDataManager.swift
//  TinkoffChat
//
//  Created by Alexander on 22/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

class OperationDataManager: ProfileSaveLoadManager {
    
    private let profileSaveLoadManager: ProfileSaveLoadManager = LocalStorageProfileManager()
    private let serialQueue = OperationQueue()
    private let mainQueue = OperationQueue.main
    //serialQueue.maxCon
    
    func load(completion: @escaping (String?, String?, UIImage?) -> Void) {
        serialQueue.maxConcurrentOperationCount = 1
        serialQueue.addOperation {
            self.profileSaveLoadManager.load { userName, userInfo, userAvatar in
                self.mainQueue.addOperation {
                    completion(userName, userInfo, userAvatar)
                }
            }
        }
    }
    
    func save(userName: String?, userInfo: String?, userAvatar: UIImage?, completion: @escaping () -> Void) {
        serialQueue.maxConcurrentOperationCount = 1
        serialQueue.addOperation {
            self.profileSaveLoadManager.save(userName: userName, userInfo: userInfo, userAvatar: userAvatar, completion: {
                self.mainQueue.addOperation {
                    completion()
                }
            })
        }
    }
    
}
