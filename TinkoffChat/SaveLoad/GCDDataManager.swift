//
//  GCDDataManager.swift
//  TinkoffChat
//
//  Created by Alexander on 22/10/2018.
//  Copyright © 2018 Alexander. All rights reserved.

import UIKit

class GCDDataManager: ProfileSaveLoadManager {
    
    private let profileSaveLoadManager: ProfileSaveLoadManager = LocalStorageProfileManager()
    private let serialQueue = DispatchQueue(label: "ru.saveLoad.queue")
    
    func load(completion: @escaping (String?, String?, UIImage?) -> Void) {
        serialQueue.async {
            self.profileSaveLoadManager.load { userName, userInfo, userAvatar in
                DispatchQueue.main.async {
                    completion(userName, userInfo, userAvatar)
                }
            }
        }
    }
    
    func save(userName: String?, userInfo: String?, userAvatar: UIImage?, completion: @escaping () -> Void) {
        serialQueue.async {
            self.profileSaveLoadManager.save(userName: userName, userInfo: userInfo, userAvatar: userAvatar, completion: {
                DispatchQueue.main.async {
                    completion()
                }
            })
        }
    }
    
}
